/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 57);
/******/ })
/************************************************************************/
/******/ ({

/***/ 57:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(58);


/***/ }),

/***/ 58:
/***/ (function(module, exports) {

/* ========================================== 
scrollTop() >= 300
Should be equal the the height of the header
========================================== */
$(window).scroll(function () {
  if ($(window).scrollTop() >= 200) {
    $('div.dropdown').addClass('dropdown-sticky');
  } else {
    $('div.dropdown').removeClass('dropdown-sticky');
  }
});

function edit() {
  var img = $("#logo");

  if (img.attr("src") === "sources-homepage/logo/logo-white.png" && $(window).scrollTop() === 0) {
    document.getElementById("logo").src = "sources-homepage/logo/logo.png";
    $(".header").addClass("header-back");
    $(".navicon").addClass("navicon-black");
    $(".navicon").addClass("navicon-black2");
  } else if (img.attr("src") === "sources-homepage/logo/logo.png" && $(window).scrollTop() >= 1) {
    if (document.querySelector('.navicon-black2') !== null) {
      $(".navicon").removeClass("navicon-black2");
    } else {
      $(".navicon").addClass("navicon-black2");
    }
  } else {
    document.getElementById("logo").src = "sources-homepage/logo/logo-white.png";
    $(".header").removeClass("header-back");
    $(".navicon").removeClass("navicon-black");
    $(".navicon").removeClass("navicon-black2");
  }
}

$(window).scroll(function () {
  var img = $("#logo");

  if ($(window).scrollTop() > 10) {
    if (img.attr("src") === "sources-homepage/logo/logo-white.png") {
      document.getElementById("logo").src = "sources-homepage/logo/logo.png";
      $(".header").addClass("header-back");
      $(".navicon").addClass("navicon-black");
    }
  } else {
    document.getElementById("logo").src = "sources-homepage/logo/logo-white.png";
    $(".header").removeClass("header-back");
    $(".navicon").removeClass("navicon-black");
  }
});
/* Nav configuration */

$('#nav-icon1').click(function () {
  $(this).toggleClass('open');
  $('#overlay').toggleClass('open');
});
$('.overlay-content a').click(function () {
  $('#nav-icon1').toggleClass('open');
  $('#overlay').toggleClass('open');
});
/* fonctionnement caroussel */

$(document).ready(function () {
  // Activate the Carousel, but pause it from the start
  $("#carouselAlpine").carousel("pause");
  $("#prev").click(function () {
    $("#carouselAlpine").carousel("pause");
  });
  $("#next").click(function () {
    $("#carouselAlpine").carousel("pause");
  });
});

/***/ })

/******/ });