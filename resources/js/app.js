require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';
Vue.component('configuration', require('./components/App.vue'));
Vue.component('couleurs', require('./components/Couleurs.vue'));
Vue.component('jantes', require('./components/Jantes.vue'));
Vue.component('sellerie', require('./components/Sellerie.vue'));
Vue.component('equipements', require('./components/Equipements.vue'));
import Carousel3d from 'vue-carousel-3d';
 
Vue.use(Carousel3d);

const app = new Vue({
    el: '#app',
    data: {
        prix: "",
        modele: "",
        prixModele: "",
        couleur: "",
        prixCouleur: "",
        choixJantes: "",
        jantesPrix: '',
        choixSellerie: '',
        prixSellerie: "",
        equipementsModele: '',
        equipementsPrix: '',
        choixEquipements: '',
        currentView: 'configuration'
      }
});

