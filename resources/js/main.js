

/* ========================================== 
scrollTop() >= 300
Should be equal the the height of the header
========================================== */

$(window).scroll(function () {
    if ($(window).scrollTop() >= 200) {
        $('div.dropdown').addClass('dropdown-sticky');

    }
    else {
        $('div.dropdown').removeClass('dropdown-sticky');

    }
});

function edit() {
    var img = $("#logo");

    if (img.attr("src") === "sources-homepage/logo/logo-white.png" && $(window).scrollTop() === 0) {
        document.getElementById("logo").src = "sources-homepage/logo/logo.png";
        $(".header").addClass("header-back");
        $(".navicon").addClass("navicon-black")
        $(".navicon").addClass("navicon-black2")
    }
    else if (img.attr("src") === "sources-homepage/logo/logo.png" && $(window).scrollTop() >= 1) {
        if (document.querySelector('.navicon-black2') !== null) {
            $(".navicon").removeClass("navicon-black2")
        }
        else {
            $(".navicon").addClass("navicon-black2")
        }
    }
    else {
        document.getElementById("logo").src = "sources-homepage/logo/logo-white.png"
        $(".header").removeClass("header-back");
        $(".navicon").removeClass("navicon-black")
        $(".navicon").removeClass("navicon-black2")
    }
}

$(window).scroll(function () {
    var img = $("#logo");

    if ($(window).scrollTop() > 10) {
        if (img.attr("src") === "sources-homepage/logo/logo-white.png") {
            document.getElementById("logo").src = "sources-homepage/logo/logo.png";
            $(".header").addClass("header-back");
            $(".navicon").addClass("navicon-black");
        }
    }
    else {
        document.getElementById("logo").src = "sources-homepage/logo/logo-white.png"
        $(".header").removeClass("header-back");
        $(".navicon").removeClass("navicon-black");
    }
});

/* Nav configuration */

$('#nav-icon1').click(function(){
    $(this).toggleClass('open');
    $('#overlay').toggleClass('open')
  });
  
  $('.overlay-content a').click(function(){
    $('#nav-icon1').toggleClass('open');
    $('#overlay').toggleClass('open')
  });
  




/* fonctionnement caroussel */
$(document).ready(function(){
    // Activate the Carousel, but pause it from the start
    $("#carouselAlpine").carousel("pause");
    $("#prev").click(function(){
        $("#carouselAlpine").carousel("pause");
    });
    $("#next").click(function(){
        $("#carouselAlpine").carousel("pause");
    });
})
