<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Configurateur Alpine</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="{{ url('/css/app2.css') }}">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js"></script>
</head>
<body>
    <div class="hero">    
        <div class="navbar container">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <img class="logo" src="sources-homepage/logo/logo.png">
            <div class="overlay" id="overlay">
                <div class="overlay-content">
                    <img class="logo" src="sources-homepage/logo/logo-white.png">
                    <p>Configurez votre Alpine selon vos préférences en sélectionnant parmi l'ensemble des couleurs, équipements et accessoires proposés.</p>
                </div>
                <div class="burgermenu text-center container">
                    <ul class="mainnav">
                        <li class="mainnav-li">
                            <a href="#" class="mainnav-a">
                                <img src="https://cdn.group.renault.com/alp/pictograms/pin.svg.asset.svg/1527586473335.svg" alt="" class="mainnav-img">
                                Trouver votre Centre Alpine
                            </a>
                        </li>
                        <li class="mainnav-li">
                            <a href="#" class="mainnav-a3">
                                <img src="https://cdn.group.renault.com/alp/pictograms/configurate.svg.asset.svg/1527586472199.svg" alt="" class="mainnav-img">
                                VOS CONFIGURATIONS
                                <a href="" class="mainnav-a2">Rejouer votre configuration</a>
                                <a href="" class="mainnav-a mainnav-a2">Sauvegarder votre configuration</a>
                                </a>
                        </li>
                        <li class="mainnav-li">
                            <a href="#" class="mainnav-a">
                                <img style="width: 8%" src="sources-homepage/logo/Logoalpinewhite2017.png" class="mainnav-img">
                                DECOUVRIR L'UNIVERS ALPINE
                            </a>
                        </li>
                        <li class="mainnav-li">
                            <div class="mainnav-a" >
                                <i class="fab fa-3x fa-facebook padding-icone"></i>
                                <i class="fab fa-3x fa-twitter padding-icone"></i>
                                <i class="fab fa-3x fa-instagram padding-icone"></i>
                                <p>Continuez l'expérience</p>
                            </div>
                        </li>
                        <li class="">
                            <a href="#" class="tes">Mentions légales</a>
                        </li>
                        <li class="mainnav-a2">
                            <p> © Alpine 2017 - 2018</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="hero-content">
                <h6><strong>Configurez votre Alpine selon vos préférences en sélectionnant parmi l'ensemble des couleurs, équipements et accessoires proposés.</strong></h6>
            </div>
        </div>
    </div>

    <section class="select-cars">
        <div id="appstef">
            <configuration></configuration>
            <sellerie></sellerie>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/appstef.js"></script>
    <script src="js/main.js"></script>
    
</body>
</html>