
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alpine</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
    <link rel="stylesheet" href="css/app.css">
</head>

<body>
    <header class="header">
        <img id="logo" src="sources-homepage/logo/logo-white.png">
        <input class="menu-btn" type="checkbox" id="menu-btn" onclick='edit()'/>
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
            <div class="row">
                <li class="col-12 col-lg-2"><a href="#work">MODÈLE</a></li>
                <li class="col-12 col-lg-2"><a href="#about">NOTRE RÉSEAU</a></li>
                <li class="col-12 col-lg-2"><a href="#careers">L'UNIVERS ALPINE</a></li>
                <li class="col-12 col-lg-2"><a href="#contact">PRODUITS ET SERVICES</a></li>
                <li class="col-12 col-lg-2"><a href="#contact">RACING</a></li>
                <li class="col-12 col-lg-2"><a href="#contact">MÉDIA</a></li>
            </div>
            <li>
                <a href="#" class="header-float" id="icones">
                    <i class="fab fa-2x fa-facebook"></i>
                    <i class="fab fa-2x fa-instagram"></i>
                    <i class="fab fa-2x fa-twitter-square"></i>
                    <i class="fab fa-2x fa-pinterest"></i>
                    <i class="fab fa-2x fa-youtube"></i>
                    <i class="fab fa-2x fa-linkedin"></i>
                </a>
            </li>
        </ul>
    </header>

    <section id="modele">
    </section>

    <div class="dropdown">
        <a class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-flip="false"> 
        A110
        </a>
        <div id="A110-menu" class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <a class="dropdown-item" href="#agilite" type="button">Agilité absolue</a>
            <a class="dropdown-item" href="#design" type="button">Design</a>
            <a class="dropdown-item" href="#conception" type="button">Conception</a>
            <a class="dropdown-item" href="#motorisation" type="button">Motorisation</a>
            <a class="dropdown-item" href="#technologie" type="button">Technologie</a>
            <a class="dropdown-item" href="#interieur" type="button">Intérieur</a>
            <a class="dropdown-item" href="#caracteristiques_techniques" type="button">Caractéristiques techniques</a>
            <a class="dropdown-item" href="#versions" type="button">Versions</a>
            <a class="dropdown-item" href="#galerie" type="button">Galerie photos</a>
            <div class="row">
				<button type="button" class="btn btn-secondary col-6" id="configurer">CONFIGURER</button>
				<button type="button" class="btn btn-primary col-6" id="reserver">RÉSERVER</button>
			</div>
        </div>
    </div>

    <section id="agilite">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <h1>Agilité absolue</h1>
                    <h2>Poids plume, design élégant, l’A110 est la nouvelle voiture de sport signée Alpine.</h2>
                    <p>Séduisante, légère et joueuse, l'Alpine A110 se veut fidèle à l’esprit de la célèbre berlinette. Equipé d’un
                        moteur central arrière, ce coupé sport résolument moderne est agile, compact et léger. Aussi à l’aise sur
                        circuit que sur routes de montagne, l’A110 combine élégance et confort au quotidien.
                    </p>
                </div>
                <div class="col-12 col-lg-6 image-hover img-roll">
                    <img class="col-12 img-1" src="sources-homepage/A110/Presentation_desktop-1.png">
                    <img class="col-12 img-2" src="sources-homepage/A110/Presentation_desktop-1.png">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="image-hover img-zoom-in">
                <img class="col-12" src="sources-homepage/galerie/A110_LEGENDE_5.jpg">
            </div>
        </div>
    </section>

    <section id="design">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 hover05">
                    <img class="col-12 " src="sources-homepage/design/profil_legende_design-1.png">
                </div>
                <div class="col-12 col-lg-4">
                    <h1>Design</h1>
                    <p>La nouvelle A110 combine des éléments de style de la mythique berlinette à d'autres plus
                        modernes à
                        l’instar
                        des feux arrière LED en forme de « X » intégrant des clignotants à défilement. Emmenée par
                        Antony
                        Villain,
                        l’équipe de designers a su saisir l’esprit Alpine et créer un design élégant, inspiré de la
                        berlinette,
                        mais qui résistera à l’épreuve du temps.
                    </p>
                </div>
                <div class="row">
                    <div class="col-6 image-hover img-inner-shadow">
                        <img class="img-fluid" src="sources-homepage/design/duo-left.jpg">
                        <div class="layer"></div>
                    </div>
                    <div class="col-6 image-hover img-inner-shadow">
                        <img class="img-fluid" src="sources-homepage/design/duo-right.jpg">
                        <div class="layer"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-8 image-hover img-blur">
                        <img class="col-12" src="sources-homepage/design/Visuel_1_desktop.jpg">
                    </div>
                    <div class="col-12 col-lg-4 height-center">
                        <h2>Silhouette</h2>
                        <p>L’Alpine A110, c’est avant tout un dessin tendu réalisé d’un seul trait. C’est aussi une peau
                            qui
                            épouse
                            au
                            plus près les organes mécaniques, une assise et un centre de gravité abaissés, une vaste
                            lunette
                            arrière
                            galbée et des flancs creusés qui soulignent des passages de roues saillants.
                        </p>
                    </div>
                    <div class="col-12 col-lg-4 height-center ">
                        <h2>Une carrosserie aluminium</h2>
                        <p>L’aluminium est partout. Utilisé pour la carrosserie et le soubassement rivetés et soudés, ce
                            métal
                            noble
                            ultraléger assure à l'A110 une structure équilibrée et souple. Clins d'œil aux anciennes Alpine
                            :
                            les
                            flancs creusés et la nervure centrale sur le capot.
                        </p>
                    </div>
                    <div class="col-12 col-lg-8 image-hover img-roll">
                        <img class="col-12 img-1" src="sources-homepage/design/Visuel_3_desktop.jpg">
                        <img class="col-12 img-2" src="sources-homepage/design/Visuel_3_desktop.jpg">
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section id="conception">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-4 height-center">
                    <h1>Conception</h1>
                    <p>Pour une voiture sportive, le poids est un élément essentiel. La répartition des masses optimale de l’A110,
                        ses dimensions compactes et la légèreté de sa carrosserie en aluminium lui confèrent plaisir de conduite et
                        agilité.
                    </p>
                </div>
                <div class="col-12 col-lg-8 hover14">
                    <img class="col-12" src="sources-homepage/conception/conception_desktop.png">
                </div>
                <div class="col-12">
                    <div class="imagechange-3d image-hover hover">
                        <div class="imagechange-3d-inner">
                            <div class="imgchange-1">
                                <img class="img-fluid" src="sources-homepage/conception/alpine-bone.jpg">
                            </div>
                            <div class="imgchange-2">
                                <img class="img-fluid" src="sources-homepage/conception/alpine-skin.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="container">
                    <div class="row">   
                        <div class="col-12 col-lg-8">
                            <img class="col-12" src="sources-homepage/conception/visuel_legerete_2_desktop.jpg">
                        </div>  
                        <div class="col-12 col-lg-4 height-center">
                            <h2>Robustesse et légèreté</h2>
                            <p>L’A110 tire de sa structure en aluminium robustesse, extrême agilité et maniabilité. Le design épuré et les
                                éléments de carrosserie rivetés et soudés font de nouveau gagner en légèreté et en robustesse.
                            </p>   
                        </div>                
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="motorisation">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 height-center">
                    <h1>Motorisation</h1>
                    <p>Placé en position centrale arrière, le moteur turbocompressé 4 cylindres fait battre le cœur de l’A110.
                        Réglé par les ingénieurs Alpine pour offrir une meilleure réponse à l'accélération, le moteur à injection
                        directe 1,8 L. joue la carte de la performance. La signature sonore sportive ajoute quant à elle une part
                        d’émotion.
                    </p>
                </div>
                <div class="col-12 col-lg-6">
                    <video class="col-12" src="sources-homepage/motorisation/MOTEUR_CINEMAGRAPH-.mov" autoplay="true" loop></video>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6 height-center">
                        <h2>Performances</h2>
                        <p>La légèreté de la structure associée à un moteur turbocompressé font de l’A110 un modèle ultra performant
                            doté d’un rapport poids/puissance de 4,29 kg/ch. La preuve en est, l’A110 peut passer de 0 à 100 km/h en
                            seulement 4,5 secondes.
                        </p>
                    </div>
                    <img class="col-12 col-lg-6" src="sources-homepage/caractristiques/performance_desktop.png">
                </div>
                <div class="row">
                    <img class="col-12 col-lg-6" src="sources-homepage/caractristiques/transmission_desktop.png">
                    <div class="col-12 col-lg-6 height-center">
                        <h2>Transmission</h2>
                        <p>Assuré par une boîte à double embrayage, le système de transmission garantit des passages de vitesse fluides
                            et rapides. Les palettes en aluminium permettent au conducteur un contrôle parfait du véhicule.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="technologie">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-4 height-center">
                    <h1>Technologie</h1>
                    <p>La nouvelle A110 couple les singularités propres à Alpine aux dernières technologies. La boîte de vitesse
                        automatique et le choix entre trois modes de conduite permettent une meilleure appropriation du véhicule.
                    </p>
                </div>
                <div class="col-12 col-lg-8 image-hover img-zoom-in">
                    <img class="col-12" src="sources-homepage/technologie/suspension_desktop.png">
                </div>
            </div>
            <div class="row">
                <img class="col-12" src="sources-homepage/technologie/Technical-front-wheel-A110_mobile.jpg">
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 height-center">
                    <h2>Suspension et freins</h2>
                    <p>En plus de sa structure légère, l’A110 tient son agilité et sa précision de pilotage de son système de
                        suspension à double triangulation. Un choix qui lui confère meilleure tenue de route et stabilité sur des
                        surfaces irrégulières. Les freins de l'équipementier Brembo offrent ce qui se fait de mieux en termes
                        d’endurance et de distance de freinage, que ce soit sur circuit ou dans les lacets d’une route de montagne.
                    </p>
                </div>
                <div class="col-12 col-lg-8 hover14">
                    <img class="col-12" src="sources-homepage/technologie/disque_frein_desktop.jpg">
                </div>
            </div>
        </div>
    </section>

    <section id="interieur">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 img-blur">
                    <img class="col-12" src="sources-homepage/interieur/interieur_desktop.png">
                </div>
                <div class="col-12 col-lg-4 height-center">
                    <h1>Intérieur</h1>
                    <p>A l’intérieur, l’émotion vient du contraste entre les matières chaudes et les matières froides. Les éléments
                        en aluminium apparents se marient avec du cuir. Le carbone complète l’ambiance sportive. La présence d’une
                        console centrale surélevée et de sièges baquets légers vont de paire avec l’esprit de légèreté de la
                        structure. La climatisation et la connexion au smartphone garantissent eux liberté et confort.
                    </p>
                </div>
            </div>
        </div>
        <div class="parallax"></div>  
        <div class="container"> 
            <div class="row">
                <div class="col-12 col-lg-4 height-center">     
                    <h2>Les sièges</h2>
                    <p>Avec seulement 13,1 kg chacun, les sièges baquet offrent un confort maximal en piste et participent de
                        l’extrême légèreté de l’A110. Des sièges six-voies réglables, toujours très légers, sont également proposés
                        en option.
                    </p>
                </div>
                <div class="col-12 col-lg-8 image-hover img-zoom-in">
                    <img class="col-12" src="sources-homepage/interieur/3_sieges_desktop.jpg">
                </div>
            </div>
        </div>
    </section>

    <section id="caracteristiques_techniques">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <img class="col-12" src="sources-homepage/caractristiques/dimensions-tech.png">
                </div>
                <div class="col-12 col-lg-4 height-center">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row"></th>
                                <td style="color: #005bbb">Puissance moteur</td>
                                <td style="color: black; font-weight: bold">252 ch (185 kw)</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td style="color: #005bbb">Accélération de 0 à 100 km/h</td>
                                <td style="color: black; font-weight: bold">4,5 secondes</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td style="color: #005bbb">Vitesse maximale (limitée éléctroniquement)</td>
                                <td style="color: black; font-weight: bold">250 km/h</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td style="color: #005bbb">Consommation en cycle mixte*</td>
                                <td style="color: black; font-weight: bold">6,4 l/100</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td style="color: #005bbb">Émissions CO2 en cycle mixte*</td>
                                <td style="color: black; font-weight: bold">144 g/km</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td style="color: #005bbb">Boîte de vitesse</td>
                                <td style="color: black; font-weight: bold">Automatique double embrayage à 7 rapports</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section id="versions">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1  class="text-center" style="color: white">Versions de l'A110</h1>
                    <div id="carouselAlpine" class="carousel slide" data-interval="false">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselAlpine" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselAlpine" data-slide-to="1"></li>
                            <li data-target="#carouselAlpine" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img id="alpine_pe" class="d-block" src="sources-homepage/versions/ALPINE-PE.png" alt="A110 Première edition">
                            </div>
                            <div class="carousel-item">
                                <img id="alpine_pure" class="d-block" src="sources-homepage/versions/ALPINE-PURE-1.png" alt="A110 Pure">
                            </div>
                            <div class="carousel-item">
                                <img id="alpine_legende" class="d-block" src="sources-homepage/versions/ALPINE-LEGENDE-1.png" alt="A110 Légende">
                            </div>
                        </div>
                        <a id="prev" class="carousel-control-prev" href="#carouselAlpine" role="button" data-slide="prev">
                            <i class="fas fa-2x fa-chevron-circle-left" aria-hidden="true"></i>
                        </a>
                        <a id="next" class="carousel-control-next" href="#carouselAlpine" role="button" data-slide="next">
                            <i class="fas fa-2x fa-chevron-circle-right" aria-hidden="true"></i>
                        </a>
                    </div>

                    <section id="galerie">
                        <img class="col-12" src="sources-homepage/galerie/A110_PE_1.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_PE_7.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_PE_9.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_PURE_4.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_PURE_6.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_PURE_8.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_LEGENDE_1.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_LEGENDE_5.jpg">
                        <img class="col-12" src="sources-homepage/galerie/A110_LEGENDE_9.jpg">
                    </section>
                </div>
            </div>
        </div>
	</section>

    <footer>
        <div class="container">
            <div class="row">
                <a href="#" class="col-5 offset-1">/Nous contacter</a>
				<a href="#" class="col-5 offset-1">/Media</a>
				<a href="#" class="col-5 offset-1">/Mentions légales</a>
				<a href="#" class="col-5 offset-1">/Cookies </a>
				<a href="#" class="col-5 offset-1">/S'abonner à la newsletter</a>
            </div>
            <div class="row">
                <i class="col-2 fab fa-2x fa-facebook back-white"></i>
                <i class="col-2 fab fa-2x fa-instagram back-white"></i>
                <i class="col-2 fab fa-2x fa-twitter-square back-white"></i>
                <i class="col-2 fab fa-2x fa-pinterest back-white"></i>
                <i class="col-2 fab fa-2x fa-youtube back-white"></i>
                <i class="col-2 fab fa-2x fa-linkedin back-white"></i>
            </>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="js/main.js"></script>

</body>

</html>